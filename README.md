FX Essentials
=============

Some utilities to make writing JavaFX programs easier.

Written in Kotlin, but Java programmers may also make use of some of this code.
Other parts (such as extension functions) won't work so well from Java.

Key Features
------------

### PosixParameters
In JavaFX 8 Application.Parameters are horribly broken!
It does NOT treat the special posix argument "--" correctly.
How would you pass a file called "--nasty" as an unnamed parameter?
You can't!

PosixParameters fixes this, parsing "--" correctly.

It also adds support for flags in the form :

    --flag

and also multiple single letter flags mashed together :

    -abc

where a, b anc c are three separate flags.

### ApplicationAction
Define all of your application's actions in one place, including shortcuts,
labels, icons.
Then create buttons (of various types) and menu items based on this data.

Without this, my applications would be a mess of shortcuts, hard-coded in
multiple places, making it hard to spot slashing key code combinations.

Related classes : AllActions, ActionGroup, ActionHandler

### CustomisableActions

Create toolbars/menus, which can be customised by the end user.
Requires ApplicationActions (see above).

### requestFocusSoon()
The normal use of requestFocus() often doesn't work on nodes which have
just been added to the scene graph (because the node's scene property
isn't set straight away).

### whenXXX()
Node.whenSceneSet, Node.whenSkinSet and Node.whenFocusSet.
Perform an action either now, or as soon as the property has been set.

### focusNext()
Focus on the next node (as if tab were pressed). It does this by
walking through the graph of Nodes, looking for the next one that
accepts keyboard focus.

Node.fireTabToFocusNext() does a similar thing more succinctly, but
assumes the control won't eat the Tab key event.

### Various convenience extension functions

ToggleButton.selectedWith(Property<Boolean>)

Node.disableIf(ObservableValue<Boolean>) ,
MenuItem.disableIf(ObservableValue<Boolean>)

### FileChooser.showSaveDialogWithExtension

The FileChooser happily accepts a filename without an extension
(and doesn't even give the user any idea what the extension should be!)
This fixes it, by adding the extension if the user doesn't add it.

Example Application
-------------------

I've included an example application which demonstrates some
features found in this project.
