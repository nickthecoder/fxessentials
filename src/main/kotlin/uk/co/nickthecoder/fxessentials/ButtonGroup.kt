package uk.co.nickthecoder.fxessentials

import javafx.application.Application
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.stage.Stage

/**
 * Contains Buttons, which are laid out horizontally without any space between them, and with rounded corners
 * only on the outer most edges. This is particularly useful for radio-style buttons.
 *
 * NOTE. This needs the fxessentials.css stylesheet. Add it using the extension function :
 *
 *      myScene.addFxEssentialsCSS()
 *
 */
class ButtonGroup(vararg buttons: ButtonBase) : Control() {

    private val itemsList = FXCollections.observableArrayList<ButtonBase>()
    val items: ObservableList<ButtonBase> = itemsList

    init {
        items.addAll(buttons)
    }

    override fun createDefaultSkin() = ButtonGroupSkin(this)

    class ButtonGroupSkin(private val control: ButtonGroup) : SkinBase<ButtonGroup>(control) {
        val hbox = HBox()

        var first: ButtonBase? = null
        var last: ButtonBase? = null

        init {
            hbox.children.addAll(control.items)
            children.add(hbox)
            control.items.forEach { it.styleClass.addOnce("button-in-group") }

            if (control.items.size > 1) {
                hbox.styleClass.addOnce("button-group")
            }

            // Keep hbox's children in sync with control's items.
            control.items.addListener(object : ListChangeListener<ButtonBase> {
                override fun onChanged(c: ListChangeListener.Change<out ButtonBase>) {
                    while (c.next()) {
                        if (c.wasRemoved()) {
                            for (item in c.removed) {
                                hbox.children.remove(item)
                                item.styleClass.remove("button-in-group")
                            }
                        }
                        if (c.wasAdded()) {
                            for (i in c.from until c.to) {
                                hbox.children.add(i, control.items[i])
                                hbox.children[i].styleClass.addOnce("button-in-group")
                            }
                        }
                    }

                    updateFirstAndLast()

                }
            })
            updateFirstAndLast()
        }

        private fun updateFirstAndLast() {
            if (first !== control.items.firstOrNull()) {
                first?.styleClass?.remove("first-in-group")
                first = control.items.firstOrNull()
                first?.styleClass?.add("first-in-group")
            }
            if (last !== control.items.lastOrNull()) {
                last?.styleClass?.remove("last-in-group")
                last = control.items.lastOrNull()
                last?.styleClass?.add("last-in-group")
            }
            // If there is zero or one buttons, then turn off the special styling.
            if (first === last) {
                hbox.styleClass.remove("button-group")
            } else {
                hbox.styleClass.addOnce("button-group")
            }

        }
    }

}


/**
 * A quick and dirty test application for [ButtonGroup].
 * I used it to do manual testing; adding/removing/replacing items, and ensuring that the hbox's
 * list kept in sync with the ButtonGroup's items, and the that rounded worked correctly.
 */
internal class ButtonGroupTestApp : Application() {

    override fun start(stage: Stage) {

        val buttonGroup = ButtonGroup()

        fun createButton(text: String) = Button(text).apply {
            onAction = EventHandler { buttonGroup.items.remove(this) }
        }

        val toolBar = ToolBar().apply {
            items.addAll(
                    Button("Replace").apply {
                        onAction = EventHandler { if (buttonGroup.items.isNotEmpty()) buttonGroup.items[0] = createButton("Replaced") }
                    },
                    Button("+").apply {
                        onAction = EventHandler { buttonGroup.items.add(0, createButton("Front")) }
                    },
                    Button("insert").apply {
                        onAction = EventHandler { buttonGroup.items.add(if (buttonGroup.items.isEmpty()) 0 else 1, createButton("Inserted")) }
                    },
                    Button("+").apply {
                        onAction = EventHandler { buttonGroup.items.add(createButton("End")) }
                    },
                    buttonGroup

            )
        }

        buttonGroup.items.addAll(createButton("A"), createButton("B"), createButton("C"))

        val border = BorderPane().apply {
            top = toolBar
            center = Label("Click the buttons to remove them")
            prefWidth = 600.0
            prefHeight = 300.0
        }

        with(stage) {
            scene = Scene(border).apply {
                addFxEssentialsCSS()
            }
            title = "Test Button Group"
            centerOnScreen()
            show()
        }

    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(ButtonGroupTestApp::class.java, * args)
        }
    }
}
