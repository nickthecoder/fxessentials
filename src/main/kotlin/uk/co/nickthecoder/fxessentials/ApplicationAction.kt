/*
ParaTask Copyright (C) 2017  Nick Robinson>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.fxessentials

import javafx.scene.control.Tooltip
import javafx.scene.image.Image
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.input.KeyEvent

/**
 * Each action within your application is held as an ApplicationAction.
 * The ApplicationAction can then be used to create Buttons, ToggleButtons, MenuItems etc.
 *
 * Each action has a name. This is used to find an icon, and also as a label in the GUI
 * where shortcut keys are redefined by the user. The name is also used as a label if no label
 * is specified, and the icon couldn't be found.
 *
 * A [defaultKeyCodeCombination] is used in conjunction with [ActionGroup] so that actions
 * can be performed by the user using a shortcut key (e.g. Ctrl+S for save).
 *
 * The default shortcuts can be modified by the user, using [AllActions.edit].
 *
 * A tooltip can be specified. If both a tooltip and a shortcut are specified, then the
 * final tooltip will display the shortcut in brackets after the main text.
 * i.e. tooltip="Save" will turn into "Save (ctrl+S)"
 *
 * Actions also have a label, which will often be a more human-readable version of [name].
 * When creating buttons using [ActionGroup.button] the label will be ignored if the [image] exists.
 * If you need both a label and an icon, using [ActionGroup.labelledButton] instead.
 */
open class ApplicationAction internal constructor(
    val name: String,
    val label: String? = null,
    val image: Image?,
    val defaultKeyCodeCombination: KeyCodeCombination?,
    val tooltip: String? = null,
    /**
     * Set to `false` for some special [ApplicationAction]s, to prevent them appearing in
     * the [ShortcutsEditor].
     */
    val hasAction: Boolean = true
) {

    var keyCodeCombination = defaultKeyCodeCombination

    fun revert() {
        keyCodeCombination = defaultKeyCodeCombination
    }

    fun isChanged(): Boolean = keyCodeCombination != defaultKeyCodeCombination

    open fun shortcutString(): String = keyCodeCombination?.toString() ?: ""

    open fun match(event: KeyEvent): Boolean {
        val kcc = keyCodeCombination ?: return false
        return kcc.code == event.code &&
                kcc.control.match(event.isControlDown) &&
                kcc.shift.match(event.isShiftDown) &&
                kcc.alt.match(event.isAltDown) &&
                kcc.meta.match(event.isMetaDown)

        // NOTE. The following returned true, when it shouldn't. Not sure why, but
        // KeyCombination has some weird logic which "resolves" modifier values.
        // I don't know their intent, but I want "plain and simple", not "resolved". Grr.
        // return keyCodeCombination?.match(event) == true
    }

    open fun createTooltip(): Tooltip? {
        if (tooltip == null && keyCodeCombination == null) {
            return null
        }

        val result = StringBuilder()

        val main = tooltip ?: label
        main?.let { result.append(it) }

        if (main != null && keyCodeCombination != null) {
            result.append(" (")
        }
        shortcutLabel()?.let { result.append(it) }
        if (main != null && keyCodeCombination != null) {
            result.append(")")
        }

        return Tooltip(result.toString())
    }

    /**
     * Using [keyCodeCombination?.displayText] gives horrible results for keys such as Enter.
     * It gives results such as `Ctrl+↵` which is horrible IMHO.
     * Note, the code in [KeyCodeCombination.getDisplayText] goes OUT OF ITS WAY to replace "Enter" with `"↵"`.
     * This is DESPITE a comment saying `We convert 'ENTER' to 'Enter'`, which is blatantly false. Grr.
     *
     * So now I have to undo its nasty work!
     *
     * I realise that using symbols is better for non-english speaking users,
     * so if you prefer results such as `Ctrl+↵` then use this instead :
     *
     *      this.keyCodeCombination?.displayText
     *
     * However, AFAIK, this won't give translated versions for keys such as Cancel.
     * Also the modifier keys will not be internationalised, e.g. it will return `Ctrl`, not `Strg` for
     * german users. So I don't think this is a good reason.
     *
     * If you know how to get fully internationalised keyCodeCombination strings using JavaFX 8, please let me know.
     */
    open fun shortcutLabel(): String? {
        val kcc = keyCodeCombination ?: return null
        val text = kcc.displayText
        val last = text.lastOrNull()
        val replacement = when (last) {
            '↵' -> "Enter"
            '←' -> "Left"
            '→' -> "Right"
            '↑' -> "Up"
            '↓' -> "Down"
            '-' -> "Minus"
            '+' -> "Plus"
            ',' -> "Comma"
            '.' -> "Period"
            else -> null
        }
        return if (replacement == null) {
            text
        } else {
            text.substring(0, text.length - 1) + replacement
        }
    }

    /**
     * If the shortcut key uses a key code of 0..9, then returns that number as a string,
     * otherwise return an empty string. This is useful for adding a helpful piece of text
     * to the scene, to help the user remember which shortcut key to use.
     * For example, tabs which can be selected via a shortcut may display the number within the tab's text.
     */
    open fun shortcutNumber(): String? {
        if (keyCodeCombination?.code?.isDigitKey == true) {
            return keyCodeCombination?.code?.getName()?.last()?.toString()
        }
        return null
    }
}

fun KeyCombination.ModifierValue.match(isDown: Boolean): Boolean {
    return when (this) {
        KeyCombination.ModifierValue.ANY -> true
        KeyCombination.ModifierValue.DOWN -> isDown
        KeyCombination.ModifierValue.UP -> !isDown
    }
}
