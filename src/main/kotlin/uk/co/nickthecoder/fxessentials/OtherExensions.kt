package uk.co.nickthecoder.fxessentials

import javafx.scene.Scene
import javafx.stage.FileChooser
import javafx.stage.Window
import java.io.File

/**
 * In JavaFX 8, the save dialog lets the user type in a filename, and if no file extension is typed, it
 * happily returns a File without an extension (which is then tricky to load, as it won't match the
 * corresponding showOpenDialog's filter).
 *
 * So instead, this looks at the currently selected extension filter, and append the first extension.
 * Unless this is "*" or "", in which case the FIRST extension filter's first extension is appended.
 * Unless this is also "*" or "", in which case we give up, and return the file without an extension.
 *
 * Isn't it annoying that you have to add extra boilerplate code (or use helper functions such as this),
 * because JavaFX hasn't thought about a decent user experience? Grr.
 * It's even more annoying each application will behave differently (because they won't all use the
 * same strategy that I've uses here!)
 */
fun FileChooser.showSaveDialogWithExtension(owner: Window): File? {
    val file = showSaveDialog(owner) ?: return null

    fun extensionOfFilter(filter: FileChooser.ExtensionFilter?): String? {
        val starDotExt = filter?.extensions?.firstOrNull()
        val ext = if (starDotExt == null) "" else File(starDotExt).extension
        return if (ext == "" || ext == "*") null else ext
    }

    return if (file.extension.isEmpty()) {
        val ext = extensionOfFilter(selectedExtensionFilter)
                ?: extensionOfFilter(extensionFilters.firstOrNull())

        if (ext == null) {
            file
        } else {
            File(file.path + "." + ext)
        }
    } else {
        file
    }
}

/**
 * This is particularly useful for Node's styleClass, as we often want to add an item only once.
 */
fun <E> MutableList<E>.addOnce(item: E) {
    if (!contains(item)) add(item)
}

fun Scene.addFxEssentialsCSS() {
    stylesheets.add("/uk/co/nickthecoder/fxessentials/fxessentials.css")
}