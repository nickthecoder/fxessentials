package uk.co.nickthecoder.fxessentials

import javafx.beans.property.Property
import javafx.beans.value.ObservableValue
import javafx.event.EventHandler
import javafx.scene.control.*
import javafx.scene.image.ImageView

/**
 * Created within an [ActionGroup], and combines the [ApplicationAction] with the handler code.
 *
 * These are use to create various GUI Controls sharing the same behaviour.
 * For example, an [ActionHandler] can create a [Button] and/or a [MenuItem].
 * A [ToggleHandler] can create a [ToggleButton] and a [CheckMenuItem].
 *
 * You typically do NOT create [BaseHandler] directly, and instead use the methods
 * [ActionGroup.action] and [ActionGroup.toggle].
 */
abstract class BaseHandler(
        val action: ApplicationAction,
        val disabled: ObservableValue<Boolean>?
) {

    abstract fun act();

    /**
     * To keep the code compact, various buttons (Button, ToggleButton, MenuButton etc) are all
     * set up here.
     */
    protected open fun updateButton(button: ButtonBase) {

        if (action.image == null) {
            button.text = action.label ?: action.name
        } else {
            button.graphic = ImageView(action.image)
        }

        button.tooltip = action.createTooltip()

        if (disabled != null) {
            button.disableIf(disabled)
        }

    }

    protected open fun updateMenuItem(menuItem: MenuItem) {
        menuItem.text = action.label

        action.image?.let { menuItem.graphic = ImageView(it) }
        menuItem.accelerator = action.keyCodeCombination

        if (disabled != null) {
            menuItem.disableIf(disabled)
        }
    }

}

open class ActionHandler(
        action: ApplicationAction,
        val handler: () -> Any?,
        disabled: ObservableValue<Boolean>?
) : BaseHandler(action, disabled = disabled) {

    override fun act() {
        handler()
    }

    /**
     * Creates a Button, with either an icon, or a label (but not both).
     */
    open fun button() = Button().apply { updateButton(this) }

    /**
     * Creates a Button with a label and an icon (if the image is found).
     */
    open fun labelledButton(): Button {
        return button().apply {
            text = action.label ?: action.name
        }
    }

    /**
     * Creates a MenuButton, with either an icon or a label (but not both).
     * The child items are initially empty.
     */
    open fun menuButton() = MenuButton().apply { updateButton(this) }

    /**
     * Creates a MenuButton with a label and an icon (if the image is found).
     */
    open fun labelledMenuButton(): MenuButton {
        return menuButton().apply {
            text = action.label ?: action.name
        }
    }


    /**
     * Creates a SplitMenuButton with a label or an icon (but not both).
     * The child items are initially empty.
     */
    open fun splitMenuButton() = SplitMenuButton().apply { updateButton(this) }

    /**
     * Creates a SplitMenuButton with a label and an icon (if the image is found).
     */
    open fun labelledSplitMenuButton(): SplitMenuButton {
        return splitMenuButton().apply {
            text = action.label ?: action.name
        }
    }

    open fun menu(): Menu = Menu().apply { updateMenuItem(this) }

    open fun menuItem(): MenuItem = MenuItem().apply { updateMenuItem(this) }


    override fun updateButton(button: ButtonBase) {
        super.updateButton(button)

        button.onAction = EventHandler { handler() }
    }

    override fun updateMenuItem(menuItem: MenuItem) {
        super.updateMenuItem(menuItem)
        menuItem.onAction = EventHandler { handler() }
    }

}

open class ToggleHandler(
        action: ApplicationAction,
        disabled: ObservableValue<Boolean>? = null,
        val selected: Property<Boolean>

) : BaseHandler(action, disabled = disabled) {

    override fun act() {
        selected.value = !selected.value
    }

    /**
     * Creates a ToggleButton, with either an icon, or a label (but not both).
     * If the shortcut key is pressed, then the button's state is toggled, and
     * the handler is called.
     */
    open fun toggleButton() = ToggleButton().apply { updateButton(this) }


    open fun checkMenuItem() = CheckMenuItem().apply { updateMenuItem(this) }

    /**
     * Creates a ToggleButton with a label and an icon (if the image is found).
     */
    open fun labelledToggleButton(): ToggleButton {
        return toggleButton().apply {
            text = action.label ?: action.name
        }
    }

    override fun updateButton(button: ButtonBase) {
        super.updateButton(button)

        if (button is ToggleButton) {
            button.selectedWith(selected)
        }
    }

    override fun updateMenuItem(menuItem: MenuItem) {
        super.updateMenuItem(menuItem)

        if (menuItem is CheckMenuItem) {
            menuItem.selectedWith(selected)
        }

    }
}
