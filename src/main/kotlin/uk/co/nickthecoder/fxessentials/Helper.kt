package uk.co.nickthecoder.fxessentials

import javafx.application.Application
import java.io.File

fun isLinux() = System.getProperty("os.name").startsWith("Linux")
fun isWindows() = System.getProperty("os.name").startsWith("Windows")

val NULL_FILE = File(if (isWindows()) "NUL" else "/dev/null")

fun Application.openFile(file: File) {
    if (isLinux()) {
        ProcessBuilder("xdg-open", file.absolutePath).apply {
            redirectError(NULL_FILE)
            redirectOutput(NULL_FILE)
            start()
        }
    } else {
        // There's a bug in openjfx :
        // https://bugs.openjdk.java.net/browse/JDK-8160464
        // Which is why I do the above on my Linux machines!
        hostServices.showDocument(file.absolutePath)
    }
}

fun Application.showDocument(uri: String) {
    if (isLinux()) {
        ProcessBuilder("xdg-open", uri).apply {
            redirectError(NULL_FILE)
            redirectOutput(NULL_FILE)
            start()
        }
    } else {
        // There's a bug in openjfx :
        // https://bugs.openjdk.java.net/browse/JDK-8160464
        // Which is why I do the above on my Linux machines!
        hostServices.showDocument(uri)
    }
}

/**
 * From IntelliJ this returns the directory called "out".
 * and from the command line it returns the the base directory of the application.
 * @param klass Used to find the location of the the jar file containing this class.
 */
fun distDirectory(klass: Class<*>): File? {
    try {
        val u = klass.protectionDomain.codeSource.location
        val dir = File(u.toURI()).parentFile.parentFile
        return if (dir.name == "out") {
            File(File(dir.parent, "src"), "dist")
        } else {
            dir
        }
    } catch (e: Exception) {
        return null
    }
}
