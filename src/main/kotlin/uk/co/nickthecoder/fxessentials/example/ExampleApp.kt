package uk.co.nickthecoder.fxessentials.example

import javafx.application.Application
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import javafx.stage.Stage
import uk.co.nickthecoder.fxessentials.*
import java.io.File
import java.util.Date
import java.util.prefs.Preferences

/**
 * An example application demonstrating how to use some features of FXEssentials.
 * The whole application is in a single source file. In your application, you will probably break it up.
 * For example, I always place [actions] (see `ExampleApp companion object`) in a separate source file.
 *
 * It uses [ApplicationAction]s to create the toolbar and menus.
 *
 * The toolbar uses [CustomisableActions], so that the buttons on show can be customised by the user.
 * [CustomisableActions] are NOT used for the menus. i.e. the MenuItems are fixed (but still use [ApplicationAction]s.
 *
 * The [BETTER_FOCUS] button demonstrates [requestFocusLater].
 *
 * The "red", "green", "blue" buttons are just a demonstration of [ButtonGroup]
 * (in this demo, they do nothing when pressed!)
 *
 * When the application is launched, command line parameters are parsed using [PosixParameters].
 * Their values are just printed to the console.
 */
class ExampleApp : Application() {

    override fun start(stage: Stage) {

        // Shows the difference between JavaFX's parameters, and the posix version in [PosixParameters].
        // Try running with the following command to see the differences :
        //      fxessentials  --foo=1 --foo=2 --flag -f -abc -- --bar=2 unnamed

        with(parameters) {
            println("Raw parameters     : $raw")
            println("Named parameters   : $named")
            println("Unnamed parameters : $unnamed")
        }

        println()
        val posix = parameters.createPosix()

        with(posix) {
            println("Posix Raw parameters     : $raw")
            println("Posix Named parameters   : $named")
            println("Posix Unnamed parameters : $unnamed")
            println("Posix Named Lists        : $namedLists")
        }

        if (posix.flag("help") || posix.flag("h")) {
            println("Usage : exampleApp [--help|-h]")
            System.exit(0)
        }

        actions.loadShortCuts()

        val gui = ExampleGUI()

        with(stage) {
            scene = Scene(gui).apply {
                addFxEssentialsCSS() // Needed for ButtonGroup (Used by red, green, blue buttons).
            }
            title = "Example App"
            centerOnScreen()
            show()
        }
    }

    companion object {

        /**
         * The class passed is used to find icons within the jar file. Note, the default is to look for icons
         * called `YOUR_PACKAGE/icons/NAME.png` as well as `co/uk/nickthecoder/fxessentials/icons/NAME.png`.
         */
        val actions = AllActions(ExampleApp::class.java)

        /**
         * Keeps a list of recently opened files (stored in [Preferences]).
         * The [DOCUMENT_OPEN] split menu button uses, and the "recent" menu both use it.
         */
        val recentFiles = RecentFiles(Preferences.userNodeForPackage(ExampleApp::class.java).node("recent"))

        val SEPARATOR = actions.noAction("separator")
        val GAP = actions.noAction("gap")

        val DOCUMENT_OPEN = actions.create("document-open", "Open", KeyCode.O, control = true)
        val DOCUMENT_SAVE = actions.create("document-save", "Save", KeyCode.S, control = true, shift = false)
        val DOCUMENT_SAVE_AS = actions.create("document-save-as", "Save As", KeyCode.S, control = true, shift = true)
        val EDIT_SHORTCUTS = actions.create("edit-shortcuts", "Edit Shortcuts", KeyCode.F12)
        val BETTER_FOCUS = actions.create("betterFocus", "Better Focus", KeyCode.F, control = true)
        val CLOSE_TAB = actions.create("tab.close", "Close Tab", KeyCode.W, control = true)

        val COLOR_RED = actions.create("color.red", "Red", KeyCode.DIGIT1, control = true)
        val COLOR_GREEN = actions.create("color.green", "Green", KeyCode.DIGIT2, control = true)
        val COLOR_BLUE = actions.create("color.blue", "Blue", KeyCode.DIGIT3, control = true)
        val COLOR = actions.noAction("color")

        val STATIC_SPLIT = actions.create("staticSplit")
        val DYNAMIC_SPLIT = actions.create("dynamicSplit")

        val extraActions = AllActions(ExampleApp::class.java)
        val CLEAR = extraActions.create("clear", "Clear", KeyCode.Y, control = true)
    }

    class ExampleGUI : BorderPane() {

        private val toolBar = ToolBar()

        private val fileMenu = Menu("_File")

        private val settingsMenu = Menu("_Settings")

        private val menuBar = MenuBar(fileMenu, settingsMenu)

        private val tabPane = TabPane().apply { contentVisibleWhenSelected() }

        /**
         * Determines if requestFocus() or requestFocusLater() is used when switching tabs.
         * Bound to the [CheckMenuItem] and the [ToggleButton] for [BETTER_FOCUS].
         */
        private val betterFocusProperty = SimpleBooleanProperty(false)

        /**
         * The save menu item and the save button will be disabled when there are no tabs open.
         * Notice how we only use [noTabs] ONCE when added to the action group, and menu item AND the button
         * both become disabled when there are no tabs open.
         */
        private val noTabs = Bindings.isNull(tabPane.selectionModel.selectedItemProperty())

        /**
         * Which color is selected (from the settings menu or the toolbar).
         * In this demo, the value isn't used. It's only to demonstrate how to use [ToggleChoices].
         * But we could,for example, use it for the text area's background.
         */
        private val colorProperty = SimpleObjectProperty<Color>(Color.RED)

        /**
         * Used where the [DOCUMENT_OPEN] menu button and the "recent" menu are created.
         * It is important to keep a reference to the handler the [RecentFiles] uses to open documents,
         * because it stores this using a weak reference. It does this to make protect against memory leaks
         * (which are very trickier to find).
         * Note, if you prefer, we could have defined it like this :
         *
         *      openFileHandler = { file: File -> openFile(file) }
         *
         */
        private val openFileHandler = ::openFile

        /**
         * This is how we define the code to run for each [ApplicationAction].
         */
        private val actionGroup = ActionGroup().apply {
            action(SEPARATOR) {}
            action(DOCUMENT_OPEN) { open() }
            action(DOCUMENT_SAVE, disabled = noTabs) { save() }
            action(DOCUMENT_SAVE_AS, disabled = noTabs) { saveAs() }
            action(EDIT_SHORTCUTS) { actions.edit() }
            action(CLOSE_TAB) { tabPane.selectionModel.selectedItem?.let { tabPane.tabs.remove(it) } }
            toggle(BETTER_FOCUS, selected = betterFocusProperty)

            action(COLOR) {}
            action(STATIC_SPLIT) { println("Static Split Button pressed") }
            action(DYNAMIC_SPLIT) { println("Dynamic Split Button pressed") }

            choices(colorProperty).apply {
                toggle(COLOR_RED, Color.RED)
                toggle(COLOR_GREEN, Color.GREEN)
                toggle(COLOR_BLUE, Color.BLUE)
            }
        }

        private val notesTab = Tab(
            "Notes",
            TextArea("If you close all of these tabs (Ctrl+W), the Save button should be disabled automagically ;-)")
        )

        // Let's add some extra actions, just for the last tab...
        private val extraActionsGroup = ActionGroup().apply {
            // Shortcut Ctrl+-Y will only work when the last tab is selected
            action(CLEAR) { (notesTab.content as TextArea).clear() }
            autoAttach(notesTab.content)
        }

        /**
         * Creates [Node]s for each item in the [toolBar].
         */
        private val nodeFactory = object : CustomisableItemFactory<Node> {
            override fun createItem(aa: ApplicationAction): Node? {
                with(actionGroup) {
                    return when (aa) {
                        SEPARATOR -> Separator()
                        GAP -> createHGrow()
                        DOCUMENT_OPEN -> actionGroup.splitMenuButton(DOCUMENT_OPEN)
                            .apply { recentFiles.addMenu(this, openFileHandler) }

                        DOCUMENT_SAVE -> button(DOCUMENT_SAVE)
                        BETTER_FOCUS -> toggleButton(BETTER_FOCUS)
                        COLOR -> ButtonGroup().apply {
                            items.addAll(
                                toggleButton(COLOR_RED),
                                toggleButton(COLOR_GREEN),
                                toggleButton(COLOR_BLUE)
                            )
                        }

                        // Demonstrates two ways to create a split menu button.
                        // STATIC_SPLIT builds its menu ONCE, whereas DYNAMIC_SPLIT builds the menu each time the menu appears.

                        STATIC_SPLIT -> splitMenuButton(STATIC_SPLIT).apply {
                            // This is called ONCE, when the button is created.
                            items.addAll(
                                MenuItem("Item created : ${Date()}").apply {
                                    onAction = EventHandler { println("Item 1 pressed") }
                                },
                                MenuItem("Item 2").apply { onAction = EventHandler { println("Item 2 pressed") } },
                            )
                        }

                        DYNAMIC_SPLIT -> splitMenuButton(DYNAMIC_SPLIT).apply {
                            onShowing = EventHandler {
                                // This is called each time the menu is displayed.
                                items.clear()
                                items.addAll(
                                    MenuItem("Item created : ${Date()}").apply {
                                        onAction = EventHandler { println("Item 1 pressed") }
                                    },
                                    MenuItem("Item 2").apply { onAction = EventHandler { println("Item 2 pressed") } },
                                )
                            }
                        }

                        EDIT_SHORTCUTS -> button(EDIT_SHORTCUTS)

                        else -> null
                    }
                }
            }
        }
        private val customisableToolbar = CustomisableContainer(
            actions, "Toolbar", toolBar.items,
            listOf(
                DOCUMENT_OPEN, DOCUMENT_SAVE, BETTER_FOCUS, SEPARATOR, COLOR, STATIC_SPLIT, DYNAMIC_SPLIT,
                GAP,
                EDIT_SHORTCUTS
            ),
            Preferences.userNodeForPackage(ExampleApp::class.java).node("toolbar"),
            nodeFactory
        )

        init {

            prefWidth = 700.0
            prefHeight = 300.0

            with(actionGroup) {
                fileMenu.items.addAll(
                    menuItem(DOCUMENT_SAVE),
                    menuItem(DOCUMENT_OPEN),
                    Menu("Recent").apply { recentFiles.addMenu(this, openFileHandler) }
                )

                settingsMenu.items.addAll(
                    checkMenuItem(BETTER_FOCUS),
                    menuItem(EDIT_SHORTCUTS),
                    SeparatorMenuItem(),
                    checkMenuItem(COLOR_RED),
                    checkMenuItem(COLOR_GREEN),
                    checkMenuItem(COLOR_BLUE)
                )

                top = VBox().apply { children.addAll(menuBar, toolBar) }
                center = tabPane

                with(tabPane) {
                    tabs.add(Tab("Hello", TextArea("Switch between the tabs, and see if the text area gains focus.")))
                    tabs.add(Tab("World", TextArea("Now click the 'Better Focus' button and try again")))
                    tabs.add(notesTab)

                    // Focus on the text area whenever the tab is changed.
                    // Your code wouldn't have the "if", only the version that works properly!!!
                    selectionModel.selectedItemProperty().addListener { _, _, tab ->
                        if (betterFocusProperty.value) {
                            tab?.content?.requestFocusLater() // A version of requestFocus that actually works!
                        } else {
                            tab?.content?.requestFocus() // The standard requestFocus method often fails :-(
                        }
                    }

                    // Demonstrates how to use tabPane.hideTabs extension var.
                    // Hides the header area of the tab pane when there is only one tab.
                    tabs.addListener(ListChangeListener { tabPane.hideTabs = tabPane.tabs.size < 2 })
                }
            }
        }


        private fun open() {
            System.gc()

            val file = FileChooser().apply {
                title = "Open Example File"
                extensionFilters.addAll(
                    FileChooser.ExtensionFilter("Example Files", "*.example"),
                    FileChooser.ExtensionFilter("Text Files", "*.txt"),
                    FileChooser.ExtensionFilter("All Files", "*")
                )
            }.showOpenDialog(scene.window)

            file?.let { openFile(it) }
        }

        private fun openFile(file: File) {
            tabPane.tabs.add(Tab(file.name, TextArea().apply { text = file.readText() }))
            tabPane.selectionModel.select(tabPane.tabs.size - 1)
            recentFiles.add(file)
        }

        /**
         * In this example, we aren't storing the filenames, so save is the same as saveAs
         */
        private fun save() {
            saveAs()
        }

        /**
         * Demonstrates the auto-addition of a file extension if the user doesn't choose one.
         * If they type the name "foo", and leave the filter on "Example Files", then
         * the file will be "foo.example", and if they picked "Text Files", it would be "foo.txt".
         * If they picked "All Files", then the first filter will be used, so "foo.example" ).
         */
        private fun saveAs() {
            val file = FileChooser().apply {
                title = "Save Example File"
                extensionFilters.addAll(
                    FileChooser.ExtensionFilter("Example Files", "*.example"),
                    FileChooser.ExtensionFilter("Text Files", "*.txt"),
                    FileChooser.ExtensionFilter("All Files", "*")
                )
            }.showSaveDialogWithExtension(scene.window)

            file?.let {
                println("Save to $it")
                recentFiles.add(it)
            }
        }
    }
}

fun main(vararg args: String) {
    Application.launch(ExampleApp::class.java, * args)
}
