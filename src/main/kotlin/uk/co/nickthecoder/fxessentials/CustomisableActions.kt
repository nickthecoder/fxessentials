package uk.co.nickthecoder.fxessentials

import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.control.ToolBar
import java.util.prefs.Preferences

/**
 * Adds support for customisable Menus and ToolBars.
 * Instead of hard-coding the buttons (and their order)
 */
abstract class CustomisableActions<T>(

    val actions: AllActions,
    /**
     * A label for the customisation GUI.
     * e.g. "Toolbar", "File Menu", ...
     */
    val label: String,

    /**
     * e.g. [ToolBar.items] or [Menu.items]
     */
    val parentList: ObservableList<T>,

    val defaultConfiguration: List<ApplicationAction>,
    /**
     * The [Preferences] node, used to load/save the user's preferences.
     */
    val preferences: Preferences,

    /**
     * A function, which takes an [ApplicationAction]'s name, and returns the Node/MenuItem (T)
     */
    val factory: CustomisableItemFactory<T>

) {

    /**
     * The current mapping between action names, and the MenuItem/Node.
     */
    private val map = mutableListOf<Pair<ApplicationAction, T>>()


    init {
        if (!loadPreferences()) {
            reset()
        }
    }

    private fun loadPreferences(): Boolean {
        if (!preferences.getBoolean("exists", false)) return false
        var index = 0
        var name = preferences.get("0", "")

        while (name.isNullOrBlank()) {
            actions.actions[name]?.let { add(it) }
            index++
            name = preferences.get(index.toString(), "")
        }
        return true
    }

    private fun savePreferences() {
        preferences.putBoolean("exists", true)
        for ((index, m) in map.withIndex()) {
            preferences.put(index.toString(), m.first.name)
        }
        preferences.flush()

    }

    /**
     * Resets to use [defaultConfiguration]
     */
    private fun reset() {
        parentList.clear()
        map.clear()
        for (aa in defaultConfiguration) {
            add(aa)
        }
    }

    private fun add(aa: ApplicationAction) {
        factory.createItem(aa)?.let { item ->
            parentList.add(item)
            map.add(Pair(aa, item))
        }
    }

}

class CustomisableMenu(
    allActions: AllActions,
    label: String,
    parentList: ObservableList<MenuItem>,
    defaultConfiguration: List<ApplicationAction>,
    preferences: Preferences,
    factory: CustomisableItemFactory<MenuItem>
) : CustomisableActions<MenuItem>(allActions, label, parentList, defaultConfiguration, preferences, factory)


class CustomisableContainer(
    allActions: AllActions,
    label: String,
    parentList: ObservableList<Node>,
    defaultConfiguration: List<ApplicationAction>,
    preferences: Preferences,
    factory: CustomisableItemFactory<Node>
) : CustomisableActions<Node>(allActions, label, parentList, defaultConfiguration, preferences, factory)

interface CustomisableItemFactory<T> {
    fun createItem(aa: ApplicationAction): T?
}
