package uk.co.nickthecoder.fxessentials

import javafx.scene.image.Image
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import java.util.prefs.Preferences

/**
 * Holds a map of all of your application's [ApplicationAction]s.
 *
 * By defining all of the actions in one place, it is easy to see if the default [KeyCodeCombination]s
 * clash (i.e. use the same combination for actions which could take place at the same time).
 */
open class AllActions(val baseClass: Class<*>, val resourcesPrefix: String = "icons/") {

    constructor(baseClass: Class<*>) : this(baseClass, "icons/")

    val actions = mutableMapOf<String, ApplicationAction>()

    /**
     * Finds an image within the jar files.
     * The default is to look for a resource named `[resourcesPrefix]/[name].png` in the packages of the classes :
     * [baseClass] and if one isn't found, then it also looks in the fxessentials jar for a resource named
     * 'icons/[name].png in the package of the [AllActions] class.
     */
    open fun findImage(name: String): Image? {
        val res = baseClass.getResource("$resourcesPrefix$name.png")
            ?: this::class.java.getResource("icons/$name.png")
            ?: return null
        return Image(res.toExternalForm())
    }

    open fun create(
        name: String,
        label: String? = null,
        keyCode: KeyCode? = null,
        shift: Boolean? = false,
        control: Boolean? = false,
        alt: Boolean? = false,
        meta: Boolean? = false,
        tooltip: String? = null,
        iconName: String? = null

    ): ApplicationAction {
        val aa = ApplicationAction(
            name, createLabel(name, label), findImage(iconName ?: name),
            createKeyCodeCombination(keyCode, shift, control, alt, meta),
            createToolTip(name, tooltip)
        )
        actions[name] = aa
        return aa
    }

    /**
     * Creates an [ApplicationAction], with [ApplicationAction.hasAction] = false.
     * As this will never have an action associated with it, it will NOT appear in the
     * [ShortcutsEditor].
     */
    fun noAction(
        name: String,
        label: String? = null,
        tooltip: String? = null,
        iconName: String? = null
    ): ApplicationAction {
        val aa = ApplicationAction(
            name, createLabel(name, label), findImage(iconName ?: name),
            null,
            createToolTip(name, tooltip),
            hasAction = false
        )
        actions[name] = aa
        return aa
    }

    /**
     * Override this method if you want to return tooltips in a different language.
     * The default implementation returns [tooltip].
     */
    open fun createToolTip(name: String, tooltip: String?): String? = tooltip

    /**
     * Override this method if you want to return labels in a different language.
     * The default implementation returns [label].
     */
    open fun createLabel(name: String, label: String?): String? = label

    open fun edit() {
        ShortcutsEditor(this).createStage().show()
    }

    open fun preferences(): Preferences {
        return Preferences.userNodeForPackage(baseClass).node("shortcuts")
    }

    open fun loadShortCuts() {
        val root = preferences()
        actions.values.forEach { action ->
            val pref = root.node(action.name)
            val code = pref.get("keyCode", "")
            if (code == "") {
                // Do nothing, using the default value
            } else {
                if (code == "null") {
                    action.keyCodeCombination = null
                } else {
                    try {
                        val keyCode = KeyCode.valueOf(code)
                        val shift = KeyCombination.ModifierValue.valueOf(
                            pref.get(
                                "shift",
                                KeyCombination.ModifierValue.UP.name
                            )
                        )
                        val control = KeyCombination.ModifierValue.valueOf(
                            pref.get(
                                "control",
                                KeyCombination.ModifierValue.UP.name
                            )
                        )
                        val alt =
                            KeyCombination.ModifierValue.valueOf(pref.get("alt", KeyCombination.ModifierValue.UP.name))
                        val meta =
                            KeyCombination.ModifierValue.valueOf(pref.get("meta", KeyCombination.ModifierValue.UP.name))

                        action.keyCodeCombination =
                            KeyCodeCombination(keyCode, shift, control, alt, meta, KeyCombination.ModifierValue.ANY)

                    } catch (e: Exception) {
                        // Silently ignore bad data! Oops
                    }
                }
            }
        }
    }

    open fun saveShortcuts() {
        val root = preferences()
        actions.values.forEach { action ->
            val pref = root.node(action.name)
            if (action.keyCodeCombination == action.defaultKeyCodeCombination) {
                pref.clear()
            } else {
                val kcc = action.keyCodeCombination
                if (kcc == null) {
                    pref.put("keyCode", "null")
                } else {
                    pref.put("keyCode", kcc.code.name)
                    pref.put("shift", kcc.shift.name)
                    pref.put("control", kcc.control.name)
                    pref.put("alt", kcc.alt.name)
                    pref.put("meta", kcc.meta.name)
                }
            }
            pref.flush()
        }
    }

    companion object {

        val CONTEXT_MENU = createKeyCodeCombination(KeyCode.CONTEXT_MENU)
        val FOCUS_NEXT = createKeyCodeCombination(KeyCode.TAB)
        val INSERT_TAB = createKeyCodeCombination(KeyCode.TAB, control = true)
        val ESCAPE = createKeyCodeCombination(KeyCode.ESCAPE)

        fun modifier(down: Boolean?): KeyCombination.ModifierValue =
            when {
                down == null -> KeyCombination.ModifierValue.ANY
                down -> KeyCombination.ModifierValue.DOWN
                else -> KeyCombination.ModifierValue.UP
            }

        /**
         * Creates a [KeyCodeCombination] for the given [KeyCode], and the state for the modifier keys.
         *
         * To make the code simpler, each modifier is specified as a Boolean, which has three states :
         *
         * * true : The modifier key must be down.
         * * false : The modifier key must NOT be down
         * * null : The modifier key may be up or down (See [KeyCombination.ModifierValue.ANY])
         *
         * The default value of each is false.
         *
         * Note. the "shortcut" modifier is not used, and is always ANY.
         * @param keyCode If null, then null is returned (i.e. no shortcut key).
         */
        fun createKeyCodeCombination(
            keyCode: KeyCode?,
            shift: Boolean? = false,
            control: Boolean? = false,
            alt: Boolean? = false,
            meta: Boolean? = false
        ): KeyCodeCombination? {

            keyCode ?: return null
            return KeyCodeCombination(
                keyCode,
                modifier(shift), modifier(control), modifier(alt), modifier(meta), KeyCombination.ModifierValue.ANY
            )
        }
    }


}
