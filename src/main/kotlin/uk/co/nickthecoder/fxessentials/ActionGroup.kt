package uk.co.nickthecoder.fxessentials

import javafx.beans.property.Property
import javafx.beans.value.ObservableValue
import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.Menu
import javafx.scene.control.MenuItem
import javafx.scene.control.SplitMenuButton
import javafx.scene.control.TabPane
import javafx.scene.input.KeyEvent
import java.util.*

/**
 * An ActionGroup is a collection of [ApplicationAction]s that make up one part of your GUI, along with
 * the handler for that action.
 *
 * The [ApplicationAction]s should all be defined in one place, but each part of you GUI will use a
 * subset. This is where [ActionGroup]s come into play.
 *
 * By using ActionGroup, the handler for an Application can be defined ONCE, and multiple UI elements
 * can be created. For example one ApplicationAction can be used by a Button in a ToolBar
 * as well as a MenuItem in a Menu.
 */
open class ActionGroup {

    val actionMap = mutableMapOf<ApplicationAction, BaseHandler>()

    private val keyPressedHandler = EventHandler<KeyEvent> { keyPressed(it) }
    private val keyTypedHandler = EventHandler<KeyEvent> { keyTyped(it) }

    /**
     * The default behaviour for exceptions thrown by actions is to print the stack trace.
     */
    var errorHandler: ((Exception) -> Unit)? = { e -> e.printStackTrace() }


    open fun action(
        action: ApplicationAction,
        disabled: ObservableValue<Boolean>? = null,
        handler: () -> Unit
    ): ActionHandler {
        val ah = ActionHandler(action, handler, disabled = disabled)
        actionMap[action] = ah
        return ah
    }

    open fun toggle(
        action: ApplicationAction,
        selected: Property<Boolean>,
        disabled: ObservableValue<Boolean>? = null
    ): ToggleHandler {
        val ah = ToggleHandler(action, selected = selected)
        actionMap[action] = ah
        return ah
    }

    open fun <T : Any> choices(choice: Property<T>): ToggleChoices<T> {
        return ToggleChoices(this, choice)
    }

    protected var consumeKeyTyped = false

    open fun keyPressed(event: KeyEvent) {
        try {
            for (ah in actionMap.values) {
                if (ah.action.match(event) && ah.disabled?.value != true) {

                    try {
                        ah.act()
                        event.consume()
                        break

                    } catch (e: Exception) {
                        try {
                            errorHandler?.let { it(e) }
                        } catch (e2: Exception) {
                            //Do nothing
                        }
                    }
                }
            }
        } catch (e: ConcurrentModificationException) {
            // Do nothing
        }
        consumeKeyTyped = event.isConsumed
    }

    /**
     * If a regular (non control) key activates an action, then it shouldn't perform a KEY_TYPED event too.
     * So each time [keyPressed] is called, a boolean is set to determine if the next KEY_TYPED event should be consumed.
     * For example, in my calculator application, I have an action assigned to the "=" key, and the TextArea does
     * NOT add an "=" character to the text as the KEY_TYPED event is consumed before it gets to the TextArea.
     */
    open fun keyTyped(event: KeyEvent) {
        if (consumeKeyTyped) event.consume()
    }

    /**
     * Adds an [KeyEvent] handler/filter to the node,
     * so that all of the ApplicationActions' shortcuts can tested for when a key is pressed.
     *
     * If some actions need to be a filter, and others a handler, then you will need to create
     * two [ActionGroup]s.
     */
    open fun attachTo(node: Node, filter: Boolean = false) {
        if (filter) {
            node.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedHandler)
            node.addEventFilter(KeyEvent.KEY_TYPED, keyTypedHandler)
        } else {
            node.addEventHandler(KeyEvent.KEY_PRESSED, keyPressedHandler)
            node.addEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler)
        }
    }

    /**
     * Removes the [KeyEvent] handler/filter added by [attachTo].
     */
    open fun detachFrom(node: Node) {
        node.removeEventFilter(KeyEvent.KEY_PRESSED, keyPressedHandler)
        node.removeEventHandler(KeyEvent.KEY_PRESSED, keyPressedHandler)

        node.removeEventFilter(KeyEvent.KEY_TYPED, keyTypedHandler)
        node.removeEventHandler(KeyEvent.KEY_TYPED, keyTypedHandler)
    }


    /**
     * Automatically attach whenever [target] and its ancestors are visible in the scene,
     * and detach again whenever no longer visible.
     * The [KeyEvent] handlers are added to the root of the scene, not to [target], and therefore
     * shortcut events will fire even when the focus is outside the [target] node.
     *
     * If you want to use this in conjunction with a TabPane, you must also call [TabPane.contentVisibleWhenSelected],
     * because otherwise, JavaFX considers tab contents "visible" even when the tab is not selected
     * (and therefore the shortcuts will *always* be attached).
     */
    open fun autoAttach(target: Node, filter: Boolean = false) {
        target.whenSceneSet {
            val rootNode = target.scene.root

            fun attachDetach() {
                if (target.mimicIsTreeVisible() && target.scene != null) {
                    rootNode?.let { attachTo(rootNode) }
                } else {
                    rootNode?.let { detachFrom(rootNode) }
                }
            }

            attachDetach()
            target.mimicTreeVisibleProperty().addListener { _, _, _ ->
                attachDetach()
            }

        }
    }

    fun clear() {
        actionMap.clear()
    }

    // Action Handlers

    protected fun findAction(action: ApplicationAction): ActionHandler = actionMap[action] as? ActionHandler
        ?: throw RuntimeException("ActionHandler $action not found in the ActionGroup")

    open fun button(action: ApplicationAction) = findAction(action).button()

    open fun labelledButton(action: ApplicationAction) = findAction(action).labelledButton()
    open fun menuButton(action: ApplicationAction) = findAction(action).menuButton()
    open fun labelledMenuButton(action: ApplicationAction) = findAction(action).labelledMenuButton()

    open fun splitMenuButton(action: ApplicationAction) = findAction(action).splitMenuButton()
    open fun labelledSplitMenuButton(action: ApplicationAction) = findAction(action).labelledSplitMenuButton()

    open fun splitMenuButton(action: ApplicationAction, rebuild: (SplitMenuButton) -> Unit) =
        splitMenuButton(action).apply {
            onShowing = EventHandler { rebuild(this) }
        }

    open fun labelledSplitMenuButton(action: ApplicationAction, rebuild: (SplitMenuButton) -> Unit) =
        labelledSplitMenuButton(action).apply {
            onShowing = EventHandler { rebuild(this) }
        }

    open fun menu(action: ApplicationAction) = findAction(action).menu()
    open fun menuItem(action: ApplicationAction) = findAction(action).menuItem()

    // ToggleHandlers

    protected fun findToggle(action: ApplicationAction): ToggleHandler = actionMap[action] as? ToggleHandler
        ?: throw RuntimeException("ToggleHandler $action not found in the ActionGroup")

    open fun toggleButton(action: ApplicationAction) = findToggle(action).toggleButton()
    open fun labelledToggleButton(action: ApplicationAction) = findToggle(action).labelledToggleButton()

    open fun checkMenuItem(action: ApplicationAction) = findToggle(action).checkMenuItem()

}
