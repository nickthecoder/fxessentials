package uk.co.nickthecoder.fxessentials

import javafx.beans.value.ChangeListener
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.Button
import java.lang.ref.WeakReference

/**
 * Changes the "default" property of a Button when the focus is somewhere within one of its ancestor nodes.
 * This allows multiple "default" buttons within a single Scene, and their "default" property will change
 * appropriately.
 *
 * You do not have to use this class directly, use the extension function [Button.defaultWhileFocusWithin] instead.
 *
 * Note, we use WeakReferences, to prevent memory leaks.
 */
class DefaultButtonUpdater(button: Button, ancestor: Node) {

    private val weakParent = WeakReference(ancestor)
    private val weakButton = WeakReference(button)

    /**
     * Note, this is NOT a weak reference, and therefore MUST be removed from the [Scene]'s
     * listeners otherwise a memory leak will occur. This happens in two ways. Either [remove] is called
     * manually, or it is called automatically when the scene's focus changes and either [weakParent] or
     * [weakButton] values have been gc'd.
     */
    private val changeListener = ChangeListener<Node> { _, _, newValue -> onFocusChanged(newValue) }

    var weakScene: WeakReference<Scene>? = null

    init {
        button.whenSceneSet {
            val scene = button.scene
            scene.focusOwnerProperty().addListener(changeListener)
            weakScene = WeakReference(scene)
            onFocusChanged(scene.focusOwner)
        }
    }

    private fun onFocusChanged(newValue: Node?) {
        val ancestor = weakParent.get()

        if (ancestor == null || weakButton.get() == null) {
            remove()
        } else {

            var node: Node? = newValue

            while (node != null) {
                if (node === ancestor) {
                    focusChanged(true)
                    return
                }
                node = node.parent
            }
            focusChanged(false)
        }
    }

    fun remove() {
        weakScene?.get()?.focusOwnerProperty()?.removeListener(changeListener)
    }

    private fun focusChanged(gained: Boolean) {
        val button = weakButton.get()
        if (button == null) {
            remove()
        } else {
            button.isDefaultButton = gained
        }
    }
}

/**
 * If you have a complex scene, where more than one button can be the "default" button (i.e. is activated
 * when the Enter key is pressed) depending on which node has the focus, then this will automagically
 * set and reset [Button.isDefaultButton] for you.
 *
 * For example, if you have a split pane, with two forms, each with their own "OK" button, then call :
 *
 *      okButtonLeft.defaultWhileFocusWithin( leftPane )
 *      okButtonRight.defaultWhileFocusWithin( rightPane )
 *
 * While the focus is held by the [ancestor] [Node] or any of its descendants, then
 * [Button.isDefaultButton] is set to true.
 * When a node outside of [ancestor] gains focus, then [Button.isDefaultButton] is reset to false.
 */
fun Button.defaultWhileFocusWithin(ancestor: Node): DefaultButtonUpdater {
    return DefaultButtonUpdater(this, ancestor)
}
