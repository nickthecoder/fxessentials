package uk.co.nickthecoder.fxessentials

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.*
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.Window

class ShortcutsEditor(val allActions: AllActions) : BorderPane() {

    val labelColumn = TableColumn<ActionWrapper, String>("Label").apply {
        cellValueFactory = PropertyValueFactory("label")
        prefWidth = 300.0
    }
    val shortcutColumn = TableColumn<ActionWrapper, String>("Shortcut").apply {
        cellValueFactory = PropertyValueFactory("keyCodeString")
        prefWidth = 200.0
    }

    val nameColumn = TableColumn<ActionWrapper, String>("ID").apply {
        cellValueFactory = PropertyValueFactory("name")
        prefWidth = 200.0
    }
    val tableView = TableView<ActionWrapper>().apply {
        items = FXCollections.observableArrayList(allActions.actions.values
            .filter { it.hasAction }
            .map { ActionWrapper(it) })
        columns.setAll(labelColumn, shortcutColumn, nameColumn)

        onMouseClicked = EventHandler { onMouseClickedOrReleased(it) }
    }

    init {
        center = tableView
    }

    fun onMouseClickedOrReleased(event: MouseEvent) {

        val row = tableView.selectionModel.selectedItem ?: return

        if (event.button == MouseButton.PRIMARY && event.clickCount == 2) {
            row.edit(scene.window)
            event.consume()

        } else if (event.button == MouseButton.SECONDARY && event.clickCount == 1) {
            // NOTE. isPopupTrigger always returned false, even when I used an event filter for clicked and relased.
            ContextMenu().apply {
                items.addAll(
                    MenuItem("Edit Shortcut ...").apply {
                        onAction = EventHandler { row.edit(scene.window) }
                    },
                    MenuItem("Reset").apply {
                        onAction = EventHandler { row.setKeyCode(row.action.defaultKeyCodeCombination) }
                    }
                )
                show(this@ShortcutsEditor.scene.window, event.screenX, event.screenY)
            }

            event.consume()
        }
    }

    fun createStage(): Stage {

        val resetAll = Button("Reset All").apply {
            onAction = EventHandler {
                tableView.items.forEach { row ->
                    row.setKeyCode(row.action.defaultKeyCodeCombination)
                }
            }
        }

        val done = Button("Done").apply {
            isDefaultButton = true
        }

        val buttons = ButtonBar().apply {
            padding = Insets(6.0)
            buttons.addAll(resetAll, done)
        }

        val border = BorderPane().apply {
            center = this@ShortcutsEditor
            bottom = buttons
            prefWidth = 750.0
            prefHeight = 600.0
        }

        val stage = Stage().apply {
            title = "Edit Shortcuts"
            scene = Scene(border).apply { addFxEssentialsCSS() }
            centerOnScreen()
        }

        done.onAction = EventHandler {
            stage.hide()
            allActions.saveShortcuts()
        }

        return stage
    }


    /**
     * I don't want to use properties within ApplicationAction, so this provides a wrapper for the
     * TableView.
     */
    class ActionWrapper(val action: ApplicationAction) {

        private val nameProperty = SimpleStringProperty(action.name)
        fun nameProperty() = nameProperty

        private val labelProperty = SimpleStringProperty(action.label)
        fun labelProperty() = labelProperty

        private val keyCodeStringProperty = SimpleStringProperty(action.shortcutLabel() ?: "")
        fun keyCodeStringProperty() = keyCodeStringProperty

        private val defaultKeyCodeStringProperty = SimpleStringProperty(action.shortcutLabel() ?: "")

        fun defaultKeyCodeStringProperty() = defaultKeyCodeStringProperty

        fun setKeyCode(kcc: KeyCodeCombination?) {
            action.keyCodeCombination = kcc
            keyCodeStringProperty.value = action.shortcutLabel() ?: ""
        }

        fun edit(owner: Window) {
            setKeyCode(
                KeyCodeCombinationEditor(action.label ?: action.name, action.keyCodeCombination, owner).edit(
                    action.label ?: action.name
                )
            )
        }
    }

}

class KeyCodeCombinationEditor(
    val label: String,
    var keyCodeCombination: KeyCodeCombination?,
    val owner: Window
) {

    private val NONE = "<None>"

    // NOTE. Due to a bug??? in JavaFX 8, we cannot use ComboBox<KeyCode>, because we need to add a null and Java FX 8 throws.
    val key = ComboBox<String>().apply {
        items.add(NONE)
        items.addAll(KeyCode.values().map { it.getName() })
        value = keyCodeCombination?.code?.getName() ?: NONE
    }

    val pickLabel = Label("Press a key...")

    val pickButton = Button("Pick a Key").apply {
        onAction = EventHandler { startPick() }
    }

    val control = CheckBox("Control").apply { isAllowIndeterminate = true }
    val shift = CheckBox("Shift").apply { isAllowIndeterminate = true }
    val meta = CheckBox("Meta").apply { isAllowIndeterminate = true }
    val alt = CheckBox("Alt").apply { isAllowIndeterminate = true }

    val modifiers = HBox().apply {
        spacing = 6.0
        children.addAll(control, shift, meta, alt)
    }

    val vBox = VBox().apply {
        padding = Insets(20.0)
        spacing = 10.0
        children.addAll(Label("Shortcut : $label").apply { styleClass.add("heading") }, key, pickButton, modifiers)
    }

    val ok = Button("Ok")

    val cancel = Button("Cancel")

    val buttons = ButtonBar().apply {
        padding = Insets(6.0)
        buttons.addAll(ok, cancel)
    }

    val borderPane = BorderPane().apply {
        center = vBox
        bottom = buttons
        styleClass.add("shortcuts-editor")
    }

    val stage = Stage().apply {
        title = "Edit Shortcut"
        scene = Scene(borderPane).apply { addFxEssentialsCSS() }
        centerOnScreen()
        initOwner(owner)
        initModality(Modality.WINDOW_MODAL)
    }

    init {
        keyCodeCombination?.control?.updateCheckButton(control)
        keyCodeCombination?.shift?.updateCheckButton(shift)
        keyCodeCombination?.alt?.updateCheckButton(alt)
        keyCodeCombination?.meta?.updateCheckButton(meta)
    }

    private val pickHandler = EventHandler { event: KeyEvent ->
        endPick(event)
    }

    fun startPick() {
        vBox.children[vBox.children.indexOf(pickButton)] = pickLabel

        ok.scene.addEventFilter(KeyEvent.KEY_PRESSED, pickHandler)
    }

    fun endPick(event: KeyEvent) {
        if (!event.code.isModifierKey && event.code != null) {
            vBox.children[vBox.children.indexOf(pickLabel)] = pickButton
            ok.scene.removeEventFilter(KeyEvent.KEY_PRESSED, pickHandler)
            key.value = event.code.getName()
        }
    }

    fun edit(title: String): KeyCodeCombination? {

        ok.onAction = EventHandler {
            val keyCode = if (key.value == NONE) null else KeyCode.getKeyCode(key.value)
            keyCodeCombination = if (keyCode == null) {
                null
            } else {
                KeyCodeCombination(
                    keyCode,
                    shift.toModifier(), control.toModifier(), alt.toModifier(), meta.toModifier(),
                    KeyCombination.ModifierValue.ANY
                )
            }
            stage.hide()
        }

        cancel.onAction = EventHandler {
            stage.hide()
        }

        stage.title = "Edit Shortcut"
        stage.showAndWait()

        return keyCodeCombination
    }

}

fun KeyCombination.ModifierValue.updateCheckButton(box: CheckBox) {
    box.isSelected = this == KeyCombination.ModifierValue.DOWN
    box.isIndeterminate = this == KeyCombination.ModifierValue.ANY
}

fun CheckBox.toModifier(): KeyCombination.ModifierValue =
    if (isIndeterminate) {
        KeyCombination.ModifierValue.ANY
    } else if (isSelected) {
        KeyCombination.ModifierValue.DOWN
    } else {
        KeyCombination.ModifierValue.UP
    }