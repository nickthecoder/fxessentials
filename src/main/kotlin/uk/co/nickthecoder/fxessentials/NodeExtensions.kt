package uk.co.nickthecoder.fxessentials

import javafx.application.Platform
import javafx.beans.InvalidationListener
import javafx.beans.Observable
import javafx.beans.property.BooleanProperty
import javafx.beans.property.Property
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.event.Event
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

/**
 * Some actions, such as [Node.requestFocus] only work when the node's scene property is set.
 * This property is not set as soon as you might imagine, so actions such as [Node.requestFocus]
 * fail silently.
 *
 * This method fixes the problem.
 *
 * If the scene property IS set, then the action is performed immediately.
 * Otherwise we listen for when the scene property is set, at which point we
 * remove the listener and perform the action.
 */
fun Node.whenSceneSet(action: () -> Unit) {
    if (scene == null) {
        sceneProperty().addListener(object : InvalidationListener {
            override fun invalidated(observable: Observable?) {
                if (scene != null) {
                    sceneProperty().removeListener(this)
                    action()
                }
            }
        })
    } else {
        action()
    }
}

/**
 * Performs an action when the node gains focus.
 */
fun Node.whenFocusSet(action: () -> Unit) {
    if (isFocused) {
        action()
    } else {
        focusedProperty().addListener(object : InvalidationListener {
            override fun invalidated(observable: Observable?) {
                if (isFocused) {
                    focusedProperty().removeListener(this)
                    action()
                }
            }
        })
    }
}

/**
 * Performs an action when the control's skin has been set.
 */
fun Skinnable.whenSkinSet(action: () -> Unit) {
    if (skin == null) {
        skinProperty().addListener(object : InvalidationListener {
            override fun invalidated(observable: Observable?) {
                if (skin != null) {
                    skinProperty().removeListener(this)
                    action()
                }
            }
        })
    } else {
        action()
    }
}

/**
 * Uses [whenSceneSet] to requestFocus now, or as soon as the Node's scene is set.
 */
fun Node.requestFocusSoon() {
    whenSceneSet { requestFocus() }
}

/**
 * Uses [whenSceneSet] AND [Platform.runLater] to requestFocus.
 * Sometimes [requestFocusSoon] isn't enough!
 * My "feather calculator" application has a text area inside a split pane inside a tab pane, and
 * calling this from a listener of the tab pane's selection model.selectedItemProperty did the trick.
 * [requestFocusSoon] didn't work in that case, despite the scene property being set when the
 * requestFocus() method was finally called. Grr.
 */
fun Node.requestFocusLater() {
    whenSceneSet { Platform.runLater { requestFocus() } }
}

/**
 * Fires a KeyEvent with a code of the Tab key to move the focus onto the next focusable node.
 *
 * Also consider [focusNext].
 */
fun Node.fireTabToFocusNext() {
    this.requestFocus()
    val tab = KeyEvent(null, null, KeyEvent.KEY_PRESSED, "", "\t", KeyCode.TAB, false, false, false, false)
    Event.fireEvent(this, tab)
}

/**
 * Prints all ancestor nodes to the console. Useful for debugging.
 */
fun Node.dumpAncestors() {
    var node: Node? = this
    while (node != null) {
        println(node)
        node = node.parent
    }
}

/**
 * Finds the scrollbar node.
 * Usually applied to a ScrollPane, but may be useful for other types of node.
 * Works by [Node.lookupAll] using ".scroll-bar" as the selector.
 */
fun Node.findScrollbar(orientation: Orientation): ScrollBar? {
    for (n in lookupAll(".scroll-bar")) {
        if (n is ScrollBar) {
            if (n.orientation == orientation) {
                return n
            }
        }
    }
    return null
}

/**
 * Binds the toggle button's selectedProperty bidirectionally with [prop].
 * This is merely syntactic sugar - a simpler version of :
 *
 * myNode.apply{ selectedProperty.bindBidirectional(prop) }
 *
 * @return this
 */
fun ToggleButton.selectedWith(prop: Property<Boolean>): ToggleButton {
    this.selectedProperty().bindBidirectional(prop)
    return this
}

/**
 * Binds the toggle button's selectedProperty bidirectionally with [prop].
 * This is merely syntactic sugar - a simpler version of :
 *
 * myNode.apply{ selectedProperty.bindBidirectional(prop) }
 *
 * @return this
 */
fun CheckMenuItem.selectedWith(prop: Property<Boolean>): CheckMenuItem {
    this.selectedProperty().bindBidirectional(prop)
    return this
}

fun RadioMenuItem.selectedWith(prop: Property<Boolean>): RadioMenuItem {
    this.selectedProperty().bindBidirectional(prop)
    return this
}

/**
 * Binds the Node's disableProperty uni-directionally to [ov].
 * This is merely syntactic sugar - a simpler version of :
 *
 * myNode.apply{ disableProperty().bind(prop) }
 *
 * @return this
 */
inline fun <reified T : Node> T.disableIf(ov: ObservableValue<Boolean>): T {
    this.disableProperty().bind(ov)
    return this
}

/**
 * Binds the MenuItem's disableProperty uni-directionally to [ov].
 * This is merely syntactic sugar - a simpler version of :
 *
 * myMenuItem.apply{ disableProperty().bind(prop) }
 *
 * @return this
 */
inline fun <reified T : MenuItem> T.disableIf(ov: ObservableValue<Boolean>): T {
    this.disableProperty().bind(ov)
    return this
}

/**
 * Allows HBox.setHgrow to be accessed as if it were a field on Node.
 */
var Node.hGrow: Priority
    get() = HBox.getHgrow(this)
    set(v) {
        HBox.setHgrow(this, v)
    }

/**
 * Allows VBox.setVgrow to be accessed as if it were a field on Node.
 */
var Node.vGrow: Priority
    get() = VBox.getVgrow(this)
    set(v) {
        VBox.setVgrow(this, v)
    }

fun createHGrow(priority: Priority = Priority.ALWAYS) = Pane().apply { hGrow = priority }
fun createVGrow(priority: Priority = Priority.ALWAYS) = Pane().apply { hGrow = priority }

/**
 * Uses css magic to hide the heading area of a TabPane.
 * I use this to hide the tabs, when there is only one tab, to de-clutter the GUI.
 */
var TabPane.hideTabs: Boolean
    get() = this.styleClass.contains("hideTabs")
    set(v) {
        if (v) {
            this.styleClass.addOnce("hideTabs")
        } else {
            this.styleClass.remove("hideTabs")
        }
    }

/**
 * It seems that the content of Tabs are still considered visible, when they are not selected.
 * I want the content to be considered visible only when the tab is selected.
 * I can then use Node's visible property in a meaningful way.
 *
 * @return The invalidation listener, which "keeps tabs" on the TabPane so to speak ;-)
 * As JavaFX uses weak references for such things, it is vital that your application keep
 * a reference to this, otherwise, it will be garbage collected.
 */
fun TabPane.contentVisibleWhenSelected(): InvalidationListener {
    for (tab in tabs) {
        tab.content.isVisible = tab.isSelected
    }
    val listener = InvalidationListener {
        for (tab in tabs) {
            tab.content.isVisible = tab.isSelected
        }
    }
    this.tabs.addListener(listener)
    this.selectionModel.selectedItemProperty().addListener { _, oldValue, newValue ->
        oldValue?.content?.isVisible = false
        newValue?.content?.isVisible = true
    }
    return listener
}

/**
 * JavaFX has Node.isTreeVisible, but it is not public. This mimics it in a safe way.
 * i.e. without trying to hack into the non-public parts of JavaFX.
 */
fun Node.mimicIsTreeVisible(): Boolean {
    var node: Node? = this
    while (node != null) {
        if (!node.isVisible) return false
        node = node.parent
    }
    return true
}

/**
 * JavaFX has Node.treeVisibleProperty, but it is not public. This mimics it in a safe way.
 * i.e. without trying to hack into the non-public parts of JavaFX.
 *
 * However, this implementation is currently far from perfect.
 * It only works when this node, and all of its ancestors do not change their parentage.
 * i.e. if a node is moved from one part of the scene graph to another,
 * then this will fail in an unpredictable way.
 */
fun Node.mimicTreeVisibleProperty(): BooleanProperty {
    val property = SimpleBooleanProperty(mimicIsTreeVisible())
    val changeListener = ChangeListener<Boolean> { _, _, _ ->
        property.value = this.mimicIsTreeVisible()
    }

    fun listenToAncestors(node: Node) {
        node.visibleProperty().addListener(changeListener)
        node.parent?.let { listenToAncestors(it) }
    }
    this.whenSceneSet {
        listenToAncestors(this)
    }

    return property
}
