package uk.co.nickthecoder.fxessentials

import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.control.*
import java.io.File
import java.lang.ref.WeakReference
import java.util.prefs.Preferences

/**
 * Keeps a list of recently opened files, and automatically updates the menus passed to [addMenu], whenever an
 * item is added to the list.
 * [preferences] is where the list is stored, whenever a File is added. This is often :
 *
 *      Preferences.userNodeForPackage(MyApplication::class.java).node("recent")
 *
 * In addition, [Menu]s and [MenuButton]s can be added. These menus will be updated whenever an item is
 * added to this list.
 * Note, [WeakReference]s are used when storing the [Menu]s/[MenuButton]s, as well as the handler.
 * This lets you use a single [RecentFiles] from multiple windows, and there will be no memory leaks when one or more
 * windows are closed.
 * You must, therefore, ensure that the handler has a reference, and the easiest way to do this is to pass a method,
 * such as :
 *
 *      myRecentList.addMenu( myMenu, ::openFile )
 *
 * Where openFile is a method which takes a File, and can return anything (including Unit).
 */
class RecentFiles(
        private val preferences: Preferences,
        private val maxItems: Int = 20
) {

    private val list = mutableListOf<File>()

    private val menus =
            mutableSetOf<Pair<WeakReference<ObservableList<MenuItem>>, WeakReference<(File) -> Any?>>>()

    /**
     * @return a copy of the list of files.
     */
    val items: List<File>
        get() = list.toList()


    init {
        preferences.keys().sortedBy { it.toIntOrNull() ?: 1000 }.forEachIndexed { i, key ->
            if (i <= maxItems) {
                preferences.get(key, null)?.let { value ->
                    val file = File(value)
                    if (file.exists() && file.isFile) {
                        list.add(file)
                    }
                }
            }
        }
    }

    /**
     * @param menu The menu, which is cleared and rebuilt each time a file is added.
     * @param onOpen Stored using a weak reference, so be sure to keep a reference it
     */
    fun addMenu(menu: Menu, onOpen: (File) -> Any?) {
        removeMenu(menu)
        menus.add(Pair(WeakReference(menu.items), WeakReference(onOpen)))
        menus.removeIf { it.first.get() == null }
        buildMenu(menu.items, onOpen)
    }

    /**
     * @param menu The menu, which is cleared and rebuilt each time a file is added.
     * @param onOpen Stored using a weak reference, so be sure to keep a reference it
     */
    fun addMenu(menu: MenuButton, onOpen: (File) -> Any?) {
        removeMenu(menu)
        menus.add(Pair(WeakReference(menu.items), WeakReference(onOpen)))
        menus.removeIf { it.first.get() == null }
        buildMenu(menu.items, onOpen)
    }

    fun removeMenu(menu: MenuButton) {
        menus.removeIf { it.first.get() == null || it.first.get() === menu }
    }

    fun removeMenu(menu: Menu) {
        menus.removeIf { it.first.get() == null || it.first.get() === menu }
    }

    private fun buildMenu(menuItems: ObservableList<MenuItem>, onOpen: (File) -> Any?) {
        menuItems.clear()
        if (list.isEmpty()) {
            menuItems.add(MenuItem("No recently opened files").apply { isDisable = true })
        } else {
            for (file in list) {
                val label = Label(file.name).apply {
                    tooltip = Tooltip(file.path)
                }
                menuItems.add(
                        CustomMenuItem(label).apply {
                            onAction = EventHandler { onOpen(file) }
                        }
                )
            }
        }
    }

    private fun buildMenus() {
        for ((weakMenuItemList, weakOnOpen) in menus) {
            weakMenuItemList.get()?.let { menuItems ->
                val onOpen = weakOnOpen.get()
                if (onOpen != null) {
                    buildMenu(menuItems, onOpen)
                }
            }
        }
        menus.removeIf { it.first.get() == null || it.second.get() == null }
    }

    fun add(file: File) {
        val abs = file.absoluteFile
        list.remove(abs)
        list.add(0, abs)
        if (list.size > maxItems) list.removeAt(maxItems)

        buildMenus()

        preferences.clear()
        list.forEachIndexed { i, f ->
            preferences.put(i.toString(), f.path)
        }
        preferences.flush()
    }
}
