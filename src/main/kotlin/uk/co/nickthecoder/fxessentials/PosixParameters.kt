package uk.co.nickthecoder.fxessentials

import javafx.application.Application

/**
 * The position of the "--", which indicates the end of named arguments. Anything that follows
 * should be considered an unnamed parameter.
 *
 * Alas JavaFX 8 does NOT support the POSIX standard "--" feature.
 * Therefore [Application.Parameters.getNamed] and [Application.Parameters.getUnnamed] should NOT
 * be used.
 */
fun Application.Parameters.createPosix() = PosixParameters(this)

/**
 * Wraps JavaFX parameters, so that the special parameter "--" is correctly parsed.
 * "--" indicates the end of the named arguments, and that the remainder are unnamed,
 * even if they begin with "-" or "--".
 *
 * Without this, it is impossible to create an unnamed parameter beginning with "--".
 * (So how would you delete a file called "--nasty" ? rm '--nasty' won't work, you
 * need : rm -- --nasty.
 *
 * It annoys me that these problems have been solved decades ago (in the 70s?), and yet
 * major software such as JavaFX are still screwing it up!
 *
 * Example command line :
 *
 *     fxessentials --foo=1 --foo=2 --flag -f -abc -- --bar=2 unnamed
 *
 * "flag" and "f" are named parameters with an empty value.
 *
 * "bar" is NOT a named parameter> Note "--bar=2" is an unnamed parameter.
 *
 * "--" does not appear as a named or unnamed parameter.
 *
 * -abc is considered to be three flags (i.e. three named parameters with blank values).
 *
 *  In addition, "foo" has TWO values in [namedLists]. And returns only the last value (2)
 *  when referenced from [getNamed].
 */
class PosixParameters(val base: Application.Parameters) : Application.Parameters() {

    private val endOfNamed = base.raw.indexOf("--")

    val namedLists = mutableMapOf<String, MutableList<String>>()

    private val namedMap = mutableMapOf<String, String>()

    private val unnamedList = base.raw.filterIndexed { i, s -> (!s.startsWith("-")) || i > endOfNamed }

    init {
        raw.subList(0, if (endOfNamed < 0) raw.size else endOfNamed).forEach { p ->
            val eq = p.indexOf('=')
            if (p.startsWith("--") && eq > 0) {
                // --name=value
                val name = p.substring(2, eq)
                val value = p.substring(eq + 1)
                put(name, value)
            } else if (p.startsWith("--")) {
                // --name
                val name = p.substring(2)
                put(name, "")
            } else if (p.startsWith("-")) {
                // -abc Where a, b and c are each considered a named value with an empty value.
                val names = p.substring(2)
                names.forEach { put(it.toString(), "") }
            }
        }
    }

    /**
     * Is the named flag present? Looks for a named parameter with a blank value.
     *
     * NOTE. Unlike the standard JavaFX parameters, this class allows named parameters in the form --name
     * as well as --name=value
     *
     * This also accepts single letter flags beginning with a single dash. i.e. -abc
     * is three flags named a, b and c.
     *
     * A convenience method : namedMap[name] == ""
     */
    fun flag(name: String): Boolean = namedMap[name] == ""

    /**
     * A convenience method for checking either flag. e.g. flag("h","help")
     */
    fun flag(name: String, alternateName : String): Boolean = flag(name)  || flag(alternateName)

    private fun put(name: String, value: String) {
        val list = namedLists[name] ?: (mutableListOf<String>().apply { namedLists[name] = this })
        list.add(value)
        namedMap[name] = value
    }

    override fun getRaw(): List<String> = base.raw

    override fun getNamed() = namedMap

    override fun getUnnamed() = unnamedList
}
