package uk.co.nickthecoder.fxessentials

import javafx.application.Platform
import javafx.beans.InvalidationListener
import javafx.beans.property.Property
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ObservableValue
import javafx.scene.control.CheckMenuItem
import javafx.scene.control.ToggleButton

/**
 * Allows [CheckMenuItem]s and [ToggleButton]s to act like radio buttons.
 * i.e. You can have many buttons/menu items, and only one is selected at a time.
 * Note, this does not use RadioButton or RadioMenuItem, which depend on a ToggleGroup.
 * Instead, we have the [choice] property, which holds the current state. Each [CheckMenuItem] or
 * [ToggleButton]'s selected property is updated bi-directionally via listeners.
 */
open class ToggleChoices<T : Any>(val actionGroup: ActionGroup, val choice: Property<T>) {

    open fun toggle(
            action: ApplicationAction,
            value: T,
            disabled: ObservableValue<Boolean>? = null
    ): ToggleHandler {
        val select = SimpleBooleanProperty()
        if (choice.value == value) {
            select.value = true
        }
        choice.addListener(InvalidationListener {
            select.value = (value == choice.value)
        })

        select.addListener(InvalidationListener {
            if (select.value == true) {
                choice.value = value
            } else if (choice.value == value) {
                // Oh dear, we have tried to deselect an item, this isn't allowed, so turn it back on.
                // This must be in a runLater block, because we can't change the value inside a listener.
                // (It ignores the changed state).
                Platform.runLater {
                    select.value = true
                }
            }
        })

        val ah = ToggleHandler(action, selected = select)
        actionGroup.actionMap[action] = ah
        return ah
    }

}
